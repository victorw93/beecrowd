p  = 3.14159
values = input().split(" ")
A = float(values[0])
B = float(values[1])
C = float(values[2])


tri = A * C / 2

cir = p * (C ** 2)

tra = (A + B) * C / 2

qua = B ** 2

ret = A * B

print(f"TRIANGULO: {tri:.3f}")
print(f"CIRCULO: {cir:.3f}")
print(f"TRAPEZIO: {tra:.3f}")
print(f"QUADRADO: {qua:.3f}")
print(f"RETANGULO: {ret:.3f}")