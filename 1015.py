import math

values = input().split(" ")
x1 = float(values[0])
y1 = float(values[1])

values = input().split(" ")
x2 = float(values[0])
y2 = float(values[1])


d = math.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2))

print(f"{d:.4f}")
