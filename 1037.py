X = float(input())

if 0 <= X <= 25.0000:
    print('Intervalo [0,25]')

elif 25.00001 <= X <= 50.0000:
    print('Intervalo (25,50]')

elif 50.00001 <= X <= 75.0000:
    print('Intervalo (50,75]')

elif 75.00001 <= X <= 100.0000:
    print('Intervalo (75,100]')

else:
    print('Fora de intervalo')