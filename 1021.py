N = float(input())

cem = N // 100
N = N % 100

cinq = N // 50
N = N % 50

vinte = N // 20
N = N % 20

dez = N // 10
N = N % 10

cinco = N // 5
N = N % 5

dois = N // 2
N = N % 2

m1 = N // 1
N = N % 1 + 0.00001

m2 = N // 0.5
N = N % 0.5 + 0.00001

m3 = N // 0.25
N = N % 0.25 + 0.00001

m4 = N // 0.10
N = N % 0.10 + 0.00001

m5 = N // 0.05
N = N % 0.05 + 0.00001

m6 = N // 0.01 + 0.00001
m6 = round(m6, 2)

print("NOTAS:")
print(int(cem),"nota(s) de R$ 100.00")
print(int(cinq),"nota(s) de R$ 50.00")
print(int(vinte),"nota(s) de R$ 20.00")
print(int(dez),"nota(s) de R$ 10.00")
print(int(cinco),"nota(s) de R$ 5.00")
print(int(dois),"nota(s) de R$ 2.00")

print("MOEDAS:")
print(int(m1),"moeda(s) de R$ 1.00")
print(int(m2),"moeda(s) de R$ 0.50")
print(int(m3),"moeda(s) de R$ 0.25")
print(int(m4),"moeda(s) de R$ 0.10")
print(int(m5),"moeda(s) de R$ 0.05")
print(int(m6),"moeda(s) de R$ 0.01")