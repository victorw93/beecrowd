n = 3.14159
R = float(input())

# para potencia usar **
A = n * (R**2)

# para arredondar a numeração float, mas não exibe o zero se for ultima casa decimal
# A = round(A,4)

#para exibir a quantidade de casas decimais após a virgula, incluindo o zero, usar o modelo f"texto{variavel:.numDecimalf}
# para remover o espaço automatico do print, usar o sep="" como ultimo parametro
print(f"A={A:.4f}", sep="")