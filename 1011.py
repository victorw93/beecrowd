p  = 3.14159
r = float(input())

v = 4 / 3.0 * p * (r ** 3)

print(f"VOLUME = {v:.3f}")