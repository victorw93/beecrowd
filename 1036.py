import math

a, b, c = map(float, input().split())

d = (b ** 2) - (4 * a * c)



if (d <= 0) or (a == 0):
    print("Impossivel calcular")
    
else:
    dr = math.sqrt(d)

    x1 = (-b + dr) / (2 * a)
    x2 = (-b - dr) / (2 * a)

    print(f"R1 = {x1:.5f}")
    print(f"R2 = {x2:.5f}")